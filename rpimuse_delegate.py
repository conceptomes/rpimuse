from bluepy import btle
import bitstring
import math
import numpy as np
from collections import Counter
from rpimuse_constants import *
from time import time, sleep
import datetime as dt


class RPIMuse_Delegate(btle.DefaultDelegate):
    
    startTimestamp= time()
    
    prevpacks_EEG=[-1, -1, -1, -1, -1]
    prevpack_GYRO=-1    
    prevpack_ACC=-1
    
    bufeeg_prevpacks=[-1, -1, -1, -1, -1]
    bufacc_prevpack=-1
    bufgyro_prevpack=-1
    
    bufeeg_timestamps = np.zeros(5)
    bufacc_timestamps = np.zeros(1)
    bufgyro_timestamps = np.zeros(1)

    bufeeg_data = np.zeros((12, 5));
    bufacc_data = np.zeros((3, 3)); 
    bufgyro_data=np.zeros((3, 3));
        
    scale_gyro=MUSE_GYRO_SCALE_FACTOR
    scale_acc=MUSE_ACCELEROMETER_SCALE_FACTOR 
    
    Npacksperfile = math.floor((MUSE_SECONDSPERFILE * MUSE_EEG_SAMPLING_RATE)/12)
    Nlines_EEG= Npacksperfile*12
    Nlines_ACC= Npacksperfile*3
    Nlines_GYRO= Npacksperfile*3    
    print("Will save "+str(Nlines_EEG)+" lines of EEG, "+str(Nlines_ACC)+" lines of ACC, "+str(Nlines_GYRO)+" lines of GYRO")
    
    toteeg_data = np.zeros((Nlines_EEG, 5+3));
    totacc_data = np.zeros((Nlines_ACC, 3+3)); 
    totgyro_data = np.zeros((Nlines_GYRO, 3+3));
    
    countPacks_EEG=1
    countPacks_ACC=1
    countPacks_GYRO=1
    
    samples_EEG=0;
    samples_ACC=0;    
    samples_GYRO=0;
    
 
    saveDir = [];
    fileMnemonic = [];
    countFiles_EEG =1;
    countFiles_ACC =1;
    countFiles_GYRO =1;
    
    def __init__(self, savedir,savemnemonic):
        btle.DefaultDelegate.__init__(self)
        self.saveDir = savedir
        #Construct Mnemonic
        t1=dt.datetime.now()
        datetimemnemonic = t1.strftime("%Y%m%d_%H%M%S")
        self.fileMnemonic = savemnemonic + "_" + datetimemnemonic
 
        
    def reset_prevpacks(self):
        self.prevpacks_EEG=[-1, -1, -1, -1, -1]
        self.prevpack_GYRO=-1    
        self.prevpack_ACC=-1

    def handleNotification(self, cHandle, bytedata):
        #print("EEG")
        #os.system('cls||clear')
        #print("A notification was received: %s" %cHandle)
        packetTimestamp=time()
        
        eegHandles=[32, 35,38, 41, 44];
        gyroHandle = 20
        accHandle = 23
        telemHandle = 26

        tmpbitstr = bitstring.Bits(bytes=bytedata)
        
        #===================================================================        
        if cHandle in eegHandles:
            pattern = "uint:16,uint:12,uint:12,uint:12,uint:12,uint:12,uint:12, \
                       uint:12,uint:12,uint:12,uint:12,uint:12,uint:12"
            res = tmpbitstr.unpack(pattern)
            packetIndex = res[0]
            curdata =  np.array(res[1:])
            curdata = 0.48828125 * (curdata - 2048)
             
            eegchanIndx= int((cHandle-32)/3)
            if self.bufeeg_prevpacks[eegchanIndx] != -1:
                if packetIndex-self.bufeeg_prevpacks[eegchanIndx]!=1:
                    print("MISSED EEG between"+str(packetIndex)+" - "+str(self.bufeeg_prevpacks[eegchanIndx]))
                #else:
                #    print(".")
            self.bufeeg_data[:,eegchanIndx] = curdata 
            self.bufeeg_prevpacks[eegchanIndx]=packetIndex
         
         
            if cHandle == 35: # Last EEG channel part of the packet
                
                if len(np.unique(self.bufeeg_prevpacks))>1:
                    print("OOOPPPPSSSS not all packet numbers same in EEG buffer")
                else:
                    #Put buffer in the total matrix
                    curMatStart=(self.countPacks_EEG-1)*12
                    curMatEnd=self.countPacks_EEG*12-1
                    
                    self.toteeg_data[curMatStart:curMatEnd+1,0:4+1] = self.bufeeg_data
                    
                    #Add packet Number  
                    self.toteeg_data[curMatStart:curMatEnd+1,4+1] = np.full(12,packetIndex)            

                    #Add packet arrival timestamp
                    self.toteeg_data[curMatStart:curMatEnd+1,4+2] = np.full(12,packetTimestamp)
                    
                    #Add ideal timestamp
                    # calculate index of time samples
                    ind12 = np.arange(0, 12) + self.samples_EEG
                    self.samples_EEG += 12
                    self.toteeg_data[curMatStart:curMatEnd+1,4+3] = ind12 / MUSE_EEG_SAMPLING_RATE + self.startTimestamp            
                    
                    self.countPacks_EEG+=1
                    
                    if self.countPacks_EEG > self.Npacksperfile:
                        #print("Number of EEG Packs reached - reseting")
                        self.countPacks_EEG=1
                        #WRITE to file and zero the buffer
                        file2saveEEG = self.saveDir+"/"+self.fileMnemonic+"_EEG_"+str(self.countFiles_EEG)+".xxx"
                        np.savetxt(file2saveEEG, self.toteeg_data, delimiter=",", fmt='%s')
                        self.countFiles_EEG +=1
                        self.toteeg_data = 0 * self.toteeg_data

    

         
            
        #===================================================================            
        elif cHandle == gyroHandle:
            
            pattern = "uint:16,int:16,int:16,int:16,int:16, \
                   int:16,int:16,int:16,int:16,int:16"
            res = tmpbitstr.unpack(pattern)
            packetIndex = res[0]
            if self.bufgyro_prevpack != -1:
                if packetIndex-self.bufgyro_prevpack !=1:
                    print("MISSED GYRO between"+str(packetIndex)+" - "+str(self.bufgyro_prevpack))
            self.bufgyro_prevpack = packetIndex
                    
            for index in [1, 2, 3]: #X Y Z
                chanindx=index-1
                self.bufgyro_data[0,chanindx] = self.scale_gyro * res[index]
                self.bufgyro_data[1,chanindx] = self.scale_gyro * res[index+3]
                self.bufgyro_data[2,chanindx] = self.scale_gyro * res[index+6]
         
            #Put buffer in the total matrix
            curMatStart=(self.countPacks_GYRO-1)*3
            curMatEnd=self.countPacks_GYRO*3-1

            self.totgyro_data[curMatStart:curMatEnd+1,0:2+1] = self.bufgyro_data
            
            #Add packet Number  
            self.totgyro_data[curMatStart:curMatEnd+1,2+1] = np.full(3,packetIndex)            

            #Add packet arrival timestamp
            self.totgyro_data[curMatStart:curMatEnd+1,2+2] = np.full(3,packetTimestamp)
            
            #Add ideal timestamp
            # calculate index of time samples
            ind3 = np.arange(0, 3) + self.samples_GYRO
            self.samples_GYRO += 3
            self.totgyro_data[curMatStart:curMatEnd+1,2+3] = ind3 / MUSE_GYRO_SAMPLING_RATE + self.startTimestamp            
            
            self.countPacks_GYRO+=1
            
            if self.countPacks_GYRO > self.Npacksperfile:
                #WRITE to file and zero the buffer
                #print("Number of GYRO Packs reached - reseting")
                self.countPacks_GYRO=1
                #WRITE to file and zero the buffer
                file2saveGYRO= self.saveDir+"/"+self.fileMnemonic+"_GYRO_"+str(self.countFiles_GYRO)+".xxx"
                np.savetxt(file2saveGYRO, self.totgyro_data, delimiter=",", fmt='%s')
                self.countFiles_GYRO +=1
                self.totgyro_data = 0 * self.totgyro_data


         
        #===================================================================    
        elif cHandle == accHandle:
            pattern = "uint:16,int:16,int:16,int:16,int:16, \
                   int:16,int:16,int:16,int:16,int:16"
            res = tmpbitstr.unpack(pattern)
            packetIndex = res[0]
            if self.bufacc_prevpack != -1:
                if packetIndex-self.bufacc_prevpack !=1:
                    print("MISSED ACC between"+str(packetIndex)+" - "+str(self.bufacc_prevpack))
            self.bufacc_prevpack = packetIndex
  
                      
            for index in [1, 2, 3]: #X Y Z
                chanindx=index-1
                self.bufacc_data[0,chanindx] = self.scale_acc * res[index]
                self.bufacc_data[1,chanindx] = self.scale_acc * res[index+3]
                self.bufacc_data[2,chanindx] = self.scale_acc * res[index+6]
         
            #Put buffer in the total matrix
            curMatStart=(self.countPacks_ACC-1)*3
            curMatEnd=self.countPacks_ACC*3-1

            self.totacc_data[curMatStart:curMatEnd+1,0:2+1] = self.bufacc_data
            
            #Add packet Number  
            self.totacc_data[curMatStart:curMatEnd+1,2+1] = np.full(3,packetIndex)            

            #Add packet arrival timestamp
            self.totacc_data[curMatStart:curMatEnd+1,2+2] = np.full(3,packetTimestamp)
            
            #Add ideal timestamp
            # calculate index of time samples
            ind3 = np.arange(0, 3) + self.samples_ACC
            self.samples_ACC += 3
            self.totacc_data[curMatStart:curMatEnd+1,2+3] = ind3 / MUSE_ACC_SAMPLING_RATE + self.startTimestamp            
            
            self.countPacks_ACC+=1
            
            if self.countPacks_ACC > self.Npacksperfile:
                #WRITE to file and zero the buffer
                #print("Number of ACC Packs reached - reseting")
                self.countPacks_ACC=1
                #WRITE to file and zero the buffer
                file2saveACC= self.saveDir+"/"+self.fileMnemonic+"_ACC_"+str(self.countFiles_ACC)+".xxx"
                np.savetxt(file2saveACC, self.totacc_data, delimiter=",", fmt='%s')
                self.countFiles_ACC +=1
                self.totacc_data = 0 * self.totacc_data
 
                 
        #===================================================================
        elif cHandle == telemHandle:
            pattern = "uint:16,uint:16,uint:16,uint:16,uint:16"  # The rest is 0 padding
            res = tmpbitstr.unpack(pattern)
            packetIndex = res[0]
            battery = res[1] / 512
            fuel_gauge = res[2] * 2.2
            adc_volt = res[3]
            temperature = res[4]
            #print("battery: "+str(battery))
            #print("A notification was received: %s - %d" %(cHandle,packetIndex))
            
            
            
    def cleanup(self):
        print("Cleaning up")
        if self.countPacks_EEG>1:
            endIndx=(self.countPacks_EEG-1)*12
            file2saveEEG = self.saveDir+"/"+self.fileMnemonic+"_EEG_"+str(self.countFiles_EEG)+".xxx"
            np.savetxt(file2saveEEG, self.toteeg_data[0:endIndx,:], delimiter=",", fmt='%s')

        if self.countPacks_GYRO>1:
            endIndx=(self.countPacks_GYRO-1)*3
            file2saveGYRO = self.saveDir+"/"+self.fileMnemonic+"_GYRO_"+str(self.countFiles_GYRO)+".xxx"
            np.savetxt(file2saveGYRO, self.totgyro_data[0:endIndx,:], delimiter=",", fmt='%s')

        if self.countPacks_ACC>1:
            endIndx=(self.countPacks_ACC-1)*3
            file2saveACC = self.saveDir+"/"+self.fileMnemonic+"_ACC_"+str(self.countFiles_ACC)+".xxx"
            np.savetxt(file2saveACC, self.totacc_data[0:endIndx,:], delimiter=",", fmt='%s')
            
