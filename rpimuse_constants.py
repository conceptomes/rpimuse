MUSE_NB_CHANNELS = 5
MUSE_EEG_SAMPLING_RATE = 256
MUSE_ACC_SAMPLING_RATE = 52
MUSE_GYRO_SAMPLING_RATE = 52
MUSE_SCAN_TIMEOUT = 10.5

MUSE_SECONDSPERFILE = 30


MUSE_ACCELEROMETER_SCALE_FACTOR = 0.0000610352
MUSE_GYRO_SCALE_FACTOR = 0.0074768

MUSE_BTCHARACTERISTICS={                
             'UNKNOWN1':{'valHandle':3, 'uuid': '00002a05-0000-1000-8000-00805f9b34fb'},
             'UNKNOWN2':{'valHandle':7, 'uuid': '00002a00-0000-1000-8000-00805f9b34fb'},
             'UNKNOWN3':{'valHandle':9, 'uuid': '00002a01-0000-1000-8000-00805f9b34fb'},
             'UNKNOWN4':{'valHandle':11, 'uuid': '00002a04-0000-1000-8000-00805f9b34fb'}, 
             'CONTROL':{'valHandle':14, 'uuid': '273e0001-4c4d-454d-96be-f03bac821358'},
             'UNKNOWN5':{'valHandle':17, 'uuid': '273e0008-4c4d-454d-96be-f03bac821358'}, 
             'GYRO':{'valHandle':20, 'uuid': '273e0009-4c4d-454d-96be-f03bac821358'},                 
             'ACC':{'valHandle':23, 'uuid': '273e000a-4c4d-454d-96be-f03bac821358'},
             'TELEMETRY':{'valHandle':26, 'uuid': '273e000b-4c4d-454d-96be-f03bac821358'},
             'UNKNOWN6':{'valHandle':29, 'uuid': '273e0002-4c4d-454d-96be-f03bac821358'},            
             'EEG_TP9':{'valHandle':32, 'uuid': '273e0003-4c4d-454d-96be-f03bac821358'},
             'EEG_AF7':{'valHandle':35, 'uuid': '273e0004-4c4d-454d-96be-f03bac821358'},   
             'EEG_AF8':{'valHandle':38, 'uuid': '273e0005-4c4d-454d-96be-f03bac821358'},
             'EEG_TP10':{'valHandle':41, 'uuid': '273e0006-4c4d-454d-96be-f03bac821358'},            
             'EEG_RAUX':{'valHandle':44, 'uuid': '273e0007-4c4d-454d-96be-f03bac821358'},           
             }

'''
AUTO_DISCONNECT_DELAY = 3

LSL_SCAN_TIMEOUT = 5  # How long to look for an LSL stream.
LSL_CHUNK = 12  # Chunk size.
LSL_BUFFER = 360  # Buffer length.

VIEW_SUBSAMPLE = 2
VIEW_BUFFER = 12
'''
