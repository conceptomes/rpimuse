import os
import sys
import time
from rpimuse import RPIMuse
import getopt
import subprocess

"""===================================================================="""
"""===================================================================="""
"""Input Arguments: Usefull for remote launching with Fabric python module"""

options, remainder = getopt.getopt(sys.argv[1:], ['--muse', '--savedir', '--mnemonic'],['muse=', 'savedir=', 'mnemonic=' ])
for opt, arg in options:
    if opt in ('--muse'):
        targetMUSEaddress = arg
    elif opt in ('--savedir'):
        saveDir = arg
    elif opt in ('--mnemonic'):
        saveMnemonic = arg


"""
targetMUSEaddress = "00:55:da:b0:96:06"
saveDir = "/home/pi/workspace/projects/testrpimuse/recordings/"
saveMnemonic = "TESTRECONNECT"
"""

print('---- Starting Run with :')
print(targetMUSEaddress)
print(saveDir )
print(saveMnemonic)
print('---- -----------------------')
"""===================================================================="""
"""===================================================================="""


muse=RPIMuse(targetMUSEaddress,saveDir,saveMnemonic)

muse.connect()

muse.initialise_subscriptions()

muse.turnstreamON()


startTime=time.time()
endTime=startTime+2*60*60+30*60+11

#try:
while time.time()<endTime:
    #print(str(endTime-time.time()))
    if muse.dev.waitForNotifications(0):
        # handleNotification() was called
        continue
    

muse.cleanup()

print('sleeping')
time.sleep(5)
muse.disconnect()









