from bluepy import btle
import bitstring
import os
import time
import datetime as dt
from rpimuse_constants import MUSE_BTCHARACTERISTICS 
from rpimuse_delegate import RPIMuse_Delegate

class RPIMuse():
    maxSniffingTime=60*60
    
    def __init__(self, address,savedir,savemnemonic):
        self.address = address
        self.dev = btle.Peripheral()
        
        savesubdir=savedir+address.replace(':','')
        if not os.path.exists(savesubdir):
            os.mkdir(savesubdir,0o755)

        self.delegate=RPIMuse_Delegate(savesubdir,savemnemonic)
        self.dev.setDelegate( self.delegate )        
        
        
    def connect(self):
        
        sniffTimeStart=time.time()
        sniffTimeEnd=sniffTimeStart + self.maxSniffingTime
        isConnected=False
        while (not isConnected) and (time.time()<sniffTimeEnd):
            print('sniffin for: '+self.address)
            try:
                self.dev.connect(self.address)
                isConnected = True
            except:
                isConnected = False
                print('failed to connect - trying again')
                time.sleep(1)

        if isConnected:
            print('connected')
        else:
            print('FAILED to connect - EXITING')
            exit()
                
    
    
    def disconnect(self):
        #self.terminate_subscriptions()
        #self.turnstreamOFF()
        #self.dev.disconnect()
        
        #self.dev.delegate.reset_prevpacks()       
        self.ask_reset()
        

        print('disconnected')
        
    def initialise_subscriptions(self):
        """=============================================================================="""
        self.dev.writeCharacteristic(MUSE_BTCHARACTERISTICS['EEG_TP9']['valHandle']+1, bytearray([0x1,0x0]))
        self.dev.writeCharacteristic(MUSE_BTCHARACTERISTICS['EEG_AF7']['valHandle']+1, bytearray([0x1,0x0]))
        self.dev.writeCharacteristic(MUSE_BTCHARACTERISTICS['EEG_AF8']['valHandle']+1, bytearray([0x1,0x0]))
        self.dev.writeCharacteristic(MUSE_BTCHARACTERISTICS['EEG_TP10']['valHandle']+1, bytearray([0x1,0x0]))
        self.dev.writeCharacteristic(MUSE_BTCHARACTERISTICS['EEG_RAUX']['valHandle']+1, bytearray([0x1,0x0]))
        #----------------------------------------------------
        self.dev.writeCharacteristic(MUSE_BTCHARACTERISTICS['GYRO']['valHandle']+1, bytearray([0x1,0x0]))
        self.dev.writeCharacteristic(MUSE_BTCHARACTERISTICS['ACC']['valHandle']+1, bytearray([0x1,0x0]))
        #----------------------------------------------------
        self.dev.writeCharacteristic(MUSE_BTCHARACTERISTICS['TELEMETRY']['valHandle']+1, bytearray([0x1,0x0]))
        """=============================================================================="""                      

    def terminate_subscriptions(self):
        """=============================================================================="""
        self.dev.writeCharacteristic(MUSE_BTCHARACTERISTICS['EEG_TP9']['valHandle']+1, bytearray([0x0,0x0]))
        self.dev.writeCharacteristic(MUSE_BTCHARACTERISTICS['EEG_AF7']['valHandle']+1, bytearray([0x0,0x0]))
        self.dev.writeCharacteristic(MUSE_BTCHARACTERISTICS['EEG_AF8']['valHandle']+1, bytearray([0x0,0x0]))
        self.dev.writeCharacteristic(MUSE_BTCHARACTERISTICS['EEG_TP10']['valHandle']+1, bytearray([0x0,0x0]))
        self.dev.writeCharacteristic(MUSE_BTCHARACTERISTICS['EEG_RAUX']['valHandle']+1, bytearray([0x0,0x0]))
        #----------------------------------------------------
        self.dev.writeCharacteristic(MUSE_BTCHARACTERISTICS['GYRO']['valHandle']+1, bytearray([0x0,0x0]))
        self.dev.writeCharacteristic(MUSE_BTCHARACTERISTICS['ACC']['valHandle']+1, bytearray([0x0,0x0]))
        #----------------------------------------------------
        self.dev.writeCharacteristic(MUSE_BTCHARACTERISTICS['TELEMETRY']['valHandle']+1, bytearray([0x0,0x0]))
        """=============================================================================="""  

    def turnstreamON(self):
        self.dev.writeCharacteristic(MUSE_BTCHARACTERISTICS['CONTROL']['valHandle'], bytearray([0x02, 0x64, 0x0a]))

    def turnstreamOFF(self):
        self.dev.writeCharacteristic(MUSE_BTCHARACTERISTICS['CONTROL']['valHandle'], bytearray([0x02, 0x68, 0x0a]))

    def ask_reset(self):
        self.dev.writeCharacteristic(MUSE_BTCHARACTERISTICS['CONTROL']['valHandle'], bytearray([0x03, 0x2a, 0x31, 0x0a]))
        
    def dummyprint(self):
        print("dummy")
        
    def cleanup(self):
        self.delegate.cleanup()
            
        
        
        
            
            
        
        
        
        
        
        
        
        

    