import os
import sys
import time
from rpimuse import RPIMuse
import getopt
import subprocess

"""===================================================================="""
"""===================================================================="""
"""Input Arguments: Usefull for remote launching with Fabric python module"""

options, remainder = getopt.getopt(sys.argv[1:], ['--muse', '--savedir', '--mnemonic'],['muse=', 'savedir=', 'mnemonic=' ])
for opt, arg in options:
    if opt in ('--muse'):
        targetMUSEaddress = arg
    elif opt in ('--savedir'):
        saveDir = arg
    elif opt in ('--mnemonic'):
        saveMnemonic = arg

"""
targetMUSEaddress = "00:55:da:b0:96:06"
saveDir = "/home/pi/workspace/projects/testrpimuse/recordings/"
saveMnemonic = "TESTRECONNECT"
"""

print('---- Starting Run with :')
print(targetMUSEaddress)
print(saveDir )
print(saveMnemonic)
print('---- -----------------------')
"""===================================================================="""
"""===================================================================="""





muse=RPIMuse(targetMUSEaddress,saveDir,saveMnemonic)

muse.connect()

initialFileMnemonic = muse.delegate.fileMnemonic

muse.initialise_subscriptions()

muse.turnstreamON()


startTime=time.time()
endTime=startTime+4*60*60+30*60+11
#endTime=startTime+3*60;

timeOnSameCounter = 0
prevSampTime=time.time()
prev_countPacks_EEG = muse.delegate.countPacks_EEG
#try:
while time.time()<endTime:
    #print(str(endTime-time.time()))
    #flagNotif = False
    try:
        flagNotif=muse.dev.waitForNotifications(0)
        if flagNotif:
            nowSampTime = time.time()
            if (muse.delegate.countPacks_EEG == prev_countPacks_EEG):
                timeOnSameCounter = timeOnSameCounter + (nowSampTime-prevSampTime)
                if timeOnSameCounter > 5:
                    isMuseAround=False
                    while not isMuseAround:
                        try:
                            del muse
                            muse=RPIMuse(targetMUSEaddress,saveDir,initialFileMnemonic)
                            muse.connect()
                            muse.initialise_subscriptions()
                            muse.turnstreamON()
                            isMuseAround=True
                            timeOnSameCounter = 0
                            continue
                        except:
                            print("Error - Muse does not seem to be around")
                            time.sleep(1)

            else:
                timeOnSameCounter = 0
                prev_countPacks_EEG = muse.delegate.countPacks_EEG
                
            prevSampTime = nowSampTime
            continue
   
    except:
        isMuseAround=False
        while not isMuseAround:
            try:
                del muse
                muse=RPIMuse(targetMUSEaddress,saveDir,initialFileMnemonic )
                muse.connect()
                muse.initialise_subscriptions()
                muse.turnstreamON()
                isMuseAround=True    
                continue
            except:
                print("Error - Muse does not seem to be around")
                time.sleep(1)

        

muse.cleanup()

print('sleeping')
time.sleep(5)
muse.disconnect()











